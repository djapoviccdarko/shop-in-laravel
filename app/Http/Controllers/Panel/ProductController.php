<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\PanelProduct;
use Illuminate\Support\Facades\File;
use App\Models\Product;
use App\Scopes\AvailableScope;


class ProductController extends Controller
{
    /*    public function __construct()
        {
            $this->middleware('auth')->except(['index', 'show']);
        }*/

    public function index()
    {
        //$proudcts = DB::table('products')->get();
        $products = PanelProduct::without('images')->get();
        return view('products.index')->with([
            'products' => $products,
            //'subtitle'=>'<h2>darko</h2>',
        ]);
    }

    public function create()
    {
        return view('products.create');
    }

    public function store(ProductRequest $request)
    {
        /*        $product= Product::create([
            'title'=>request()->title,
            'description'=>request()->description,
            'price'=>request()->price,
            'stock'=>request()->request,
            'status'=>request()->status,
        ]);
             if ($request->stock == 0 && $request->status == 'available') {
                    session()->flash('error', 'If available must have stock');
                    return redirect()
                        ->back()
                        ->withInput($request->all());
                }*/
        //$product = Product::create($request->all());
        $product = PanelProduct::create($request->validated());

        foreach ($request->images as $image) {
            $product->images()->create([
                'path' => 'images/' . $image->store('products', 'images'),
            ]);
        }

        return redirect()
            ->route('products.index');
    }

    public function show(PanelProduct $product)
    {
        //$product = Product::where('id',$product)->first();
        //$product = DB::table('products')->find($product);
        /*   if  (isset($product)) {
            dd($product);
        }
        return 'fail';
        dd($product);*/
        //$product = Product::findOrFail($product);
        return view('products.show')->with([
            'product' => $product,
            //'subtitle'=>'<h2>darko</h2>',
        ]);
    }

    public function edit(PanelProduct $product)
    {
        /*     $product = Product::findOrFail($product);
             return view('products.edit')->with([
                 'product' => $product,
             ]);*/

        return view('products.edit')->with([
            'product' => $product,
        ]);
    }

    public function update(PanelProduct $product, ProductRequest $request)
    {

        //$product = Product::findOrFail($product);
        $product->update($request->validated());
        if ($request->hasFile('images')) {
            foreach ($product->images as $image) {
                $path = storage_path("app/public/{$image->path}");
                File::delete($path);
                $image->delete();
            }
            foreach ($request->images as $image) {
                $product->images()->create([
                    'path' => 'images/' . $image->store('products', 'images'),
                ]);
            }
        }

        return //$product
            //redirect()->back();
            redirect()->route('products.index');
        //redirect()->action('ProductController@index');
    }

    public function destroy(PanelProduct $product)
    {
        //$product = Product::findOrFail($product);
        $product->delete();
        return redirect()->route('products.index');
    }
}
