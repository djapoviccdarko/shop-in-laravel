<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index()
    {
        //$products=Product::where('status','available')->get();
        //$products=Product::available()->get();
        \DB::connection()->enableQueryLog();
        $products=Product::all();//dodali with u modelu da prikaze samo one se slikama zbog manjeg broja upita
        return view('welcome')->with([
            'products' => $products,
        ]);
    }
}
