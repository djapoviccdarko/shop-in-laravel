<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable=[
      'tin_jmbg',
      'activity_code',
      'company_name',
      'bank_account',
      'phone_number',
      'name',
      'surname',
      'address',
      'city',
      'email',
    ];
    public function invoices()
    {
        return $this->hasMany(Invoice::class,'client_id');
    }

}
