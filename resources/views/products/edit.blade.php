@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Edit product</h1>
        <form method="POST" action="{{ route('products.update',['product'=>$product->id]) }}"  enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-row">
                <label>Title</label>
                <input type="text" class="form-control" name="title" value="{{$product->title}}">
            </div>
            <div class="form-row">
                <label>Description</label>
                <input type="text" class="form-control" name="description" value="{{$product->description}}">
            </div>
            <div class="form-row">
                <label>Price</label>
                <input type="number" class="form-control" name="price" min="1.00" step="0.01"
                       value="{{$product->price}}">
            </div>
            <div class="form-row">
                <label>Stock</label>
                <input type="number" class="form-control" name="stock" min="0" value="{{$product->stock}}">
            </div>
            <div class="form-row">
                <label>Status</label>
                <select class="custom-select mt-3" name="status">
                    <option value="available" {{$product->status=='available' ? 'selected' : ''}}>Available</option>
                    <option value="unavailable" {{$product->status=='unavailable' ? 'selected' : ''}}>Unavailable
                    </option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-4 col-form-label text-md-end">{{ __('Images') }}</label>
                <div class="custom-file">
                    <input type="file" name="images[]" accept="image/*" class="custom-file-input" multiple>
                    <label class="custom-file-label">
                        Product images
                    </label>
                </div>
            </div>
            <div class="form-row">
                <button class="btn btn-primary btn-lg mt-3" type="submit">Update product</button>
            </div>
        </form>
    </div>
@endsection
