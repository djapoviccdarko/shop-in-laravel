@extends('layouts.app')
@section('content')
    <div class="container">
    <h1>Create product</h1>
    <form
        method="POST"
        action="{{ route('products.store') }}"
        enctype="multipart/form-data"
    >
        @csrf
        <div class="form-row">
            <label>Title</label>
            <input type="text" class="form-control" name="title" value="{{old('title')}}">
        </div>
        <div class="form-row">
            <label>Description</label>
            <input type="text" class="form-control" name="description">
        </div>
        <div class="form-row">
            <label>Price</label>
            <input type="number" class="form-control" name="price" min="1.00" step="0.01">
        </div>
        <div class="form-row">
            <label>Stock</label>
            <input type="number" class="form-control" name="stock" min="0">
        </div>
        <div class="form-row">
            <label>Status</label>
            <select class="custom-select" name="status">
                <option value="" selected>Select..</option>
                <option value="available">Available</option>
                <option value="unavailable">Unavailable</option>
            </select>
        </div>
        <div class="form-row">
            <label class="col-md-4 col-form-label text-md-end">{{ __('Images') }}</label>
            <div class="custom-file">
                <input type="file" name="images[]" accept="image/*" class="custom-file-input" multiple>
                <label class="custom-file-label">
                    Product images
                </label>
            </div>
        </div>
        <div class="form-row mt-3">
            <button class="btn btn-primary btn-lg" type="submit">Create product</button>
        </div>
    </form>
    </div>
@endsection
