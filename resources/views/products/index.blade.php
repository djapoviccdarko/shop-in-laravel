@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>List of products</h1>
        @empty($products)
            <div class="alert alert-warning">
                List is empty.
            </div>
        @else
            <a class="btn btn-success mb-3" href="{{ route('products.create') }}">Create</a>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Status</th>
                    <th>Show</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    </thead>
                    <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->description }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->stock }}</td>
                            <td>{{ $product->status }}</td>
                            <td><a href="{{ route('products.show', ['product' => $product->id]) }}">Show</a></td>
                            <td><a href="{{ route('products.edit', ['product' => $product->id]) }}">Edit</a></td>
                            <td>
                                <form class="d-inline"
                                      action="{{ route('products.destroy', ['product' => $product->id]) }}"
                                      method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" name="delete">Delete</button>
                                </form>
                            </td>


                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
