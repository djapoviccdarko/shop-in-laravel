@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>List of users</h1>
        @empty($users)
            <div class="alert alert-warning">
                List of users is empty.
            </div>
        @else
            <a class="btn btn-success mb-3" href="{{ route('products.create') }}">Create</a>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Admin since</th>
                    <th>Actions</th>
                    </thead>
                    <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                {{ optional($user->admin_since)->diffForHumans() ?? 'Never'}}
                            </td>
                            <td>
                                <form class="d-inline"
                                      action="{{ route('users.admin.toggle', ['user' =>$user->id]) }}"
                                      method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-link">
                                        {{$user->isAdmin()?'Remove':'Make'}}
                                        Admin
                                    </button>
                                </form>
                            </td>


                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
